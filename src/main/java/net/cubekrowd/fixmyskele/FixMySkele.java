package net.cubekrowd.fixmyskele;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class FixMySkele extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getLogger().info("FixMySkele has been Enabled");
    }

    @Override
    public void onDisable() {
        getLogger().info("FixMySkele has been Disabled");
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("fixmyskele")) {// sender.hasPermission("cubekrowd.fixmyskele")
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (p.getWorld().getEnvironment().equals(World.Environment.NETHER)) {
                    World nether = p.getWorld();

                    int spawnersChanged = 0;
                    int radius = 32;
                    int playerX = p.getLocation().getBlockX();
                    int playerY = p.getLocation().getBlockY();
                    int playerZ = p.getLocation().getBlockZ();

                    for (int x = playerX - radius; x <= playerX + radius; x++) {
                        for (int z = playerZ - radius; z <= playerZ + radius; z++) {
                            for (int y = (playerY - radius); y < (playerY + radius); y++) {
                                double distance = (playerX - x) * (playerX - x) + (playerZ - z) * (playerZ - z) + ((playerY - y) * (playerY - y));
                                if (distance < radius * radius) {
                                    Block spawnerBlock = nether.getBlockAt(x, y + 2, z);
                                    if (spawnerBlock.getType() == Material.MOB_SPAWNER) {
                                        CreatureSpawner spawner = (CreatureSpawner) spawnerBlock.getState();
                                        if (spawner.getSpawnedType() == EntityType.SKELETON) {
                                            spawnersChanged++;
                                            spawner.setSpawnedType(EntityType.WITHER_SKELETON);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (spawnersChanged > 1) {
                        p.sendMessage(ChatColor.RED + "Successfully changed " + spawnersChanged + " spawners.");
                    } else if (spawnersChanged == 1) {
                        p.sendMessage(ChatColor.RED + "Successfully changed 1 spawner.");
                    } else {
                        p.sendMessage(ChatColor.RED + "No spawners have been changed.");
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "You must be in the nether to execute this command!");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You must be a player to execute this command!");
            }
        }
        return true;
    }
}